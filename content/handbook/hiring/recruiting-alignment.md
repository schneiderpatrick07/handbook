---
title: "Talent Acquisition Alignment"
description: "This page is an overview of the search team alignment and the talent acquisition platform directly responsible individual in talent acquisition operations and talent brand."
---

## Search Team Alignment by Department

| Department                    | Recruiter   | Candidate Experience Specialist    |
|--------------------------|-----------------|-------------------------------------|
| Executive          | Rich Kahn    | Michelle Jubrey  |
| Executive          | Zach Choquette   | Michelle Jubrey  |
| Enterprise Sales, AMER | Kevin Rodrigues |Fernando Khubeir |
| Enterprise Sales, EMEA | Joanna Tourne | Lerato Thipe |
| Sales, AMER | Marcus Carter | Michelle Jubrey |
| Sales/ G&A | Hannah Stewart  | Sarah Lynch |
| Commercial Sales/R&D, EMEA | Ben Cowdry | Lerato Thipe |
| Sales | Kelsey Hart  | Fernando Khubeir |
| Customer Success, EMEA | Joanna Tourne | Lerato Thipe |
| Customer Success, AMER | Barbara Dinoff |  Sarah Lynch |
| All Business, APAC | Yas Priatna  | Lerato Thipe |
| Marketing/ G&A, Global | Steph Sarff | Michelle Jubrey |
| Marketing, BDR/SDR/G&A (Global)| Caroline Rebello |  Michelle Jubrey |
| G&A | Jenna VanZutphen | Fernando Khubeir |
| Development | Mark Deubel & Sara Currie | Alice Crosbie |
| R&D   | Michelle A. Kemp | Sarah Lynch  |
| R&D | Joanna Michniewicz  |  Alice Crosbie |
| R&D, Product Management | Holly Nesselroad | Fernando Khubeir |
| R&D  | Riley Smith | Lerato Thipe  |
| R&D   | Joe Guiler | Teranay Dixon  |
| R&D | Anna Kioukis  |  Teranay Dixon |
| R&D | Maxx Snow | Teranay Dixon |
| R&D  | Seema Anand | Teranay Dixon |


For urgent requests of the Candidate Experience Specialist team, we encourage you to contact them by also tagging @CES in Slack messages and CC'ing CES@gitlab.com on emails.

## Talent Acquisition Leader Alignment

| Department                    | Leader      |
|--------------------------|-----------------|
| Talent Acquisition         | Jess Dallmar |
| Talent Brand | Devin Rogozinski |
| Talent Acquisition (Sales) | Jake Foster|
| Talent Acquisition (EMEA and APAC Sales) | Jake Foster |
| Talent Acquisition (Marketing) | Steph Sarff + Jake Foster |
| Talent Acquisition (G&A) | Steph Sarff + Jake Foster |
| Talent Acquisition (R&D) | Ursela Knezevic |
| Talent Acquisition (R&D: Customer Support & Development) | Ursela Knezevic |
| Talent Acquisition (R&D: Infrastructure/Quality, Security, Product/UX, Incubation) | Ursela Knezevic |
| Talent Acquisition (Executive) | Rich Kahn |
| Enablement | Marissa Ferber |
| Candidate Experience | Ale Ayala + Marissa Farris |

## Talent Acquisition Platform Directly Responsible Individual

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Glassdoor | Admin  | Devin Rogozinski |
| Glassdoor | Responding to Reviews  | Devin Rogozinski |
| Glassdoor | Content Management | Devin Rogozinski |
| LinkedIn | Admin - Recruiter  | Devin Rogozinski |
| LinkedIn | Seats | Devin Rogozinski |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Talent Acquisition | Devin Rogozinski |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Devin Rogozinski |
