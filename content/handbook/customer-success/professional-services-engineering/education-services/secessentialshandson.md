---
title: "GitLab Security Essentials - Hands-On Lab"
description: "This Hands-On Guide walks you through the lab exercises used in the GitLab Security Essentials course."
---

# GitLab Security Essentials

## Lab Guides

> **We are transitioning to the latest version of this course.** If your group URL starts with `https://spt.gitlabtraining.cloud`, please use the v15 instructions.

| Lab Guide | Version 15 | Version 16 |
|-----------|------------|------------|
| Lab 1: Configure SAST, Secret Detection, and DAST | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson1.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson1) |
| Lab 2: Enable and Configure Dependency Scanning | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson2.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson2) |
| Lab 3: Enable and Configure Container Scanning | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson3.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson3) |
| Lab 4: Enable and Configure License Compliance | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson4.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson4) |
| Lab 5: Enable and Configure Coverage-Guided Fuzz Testing | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson5.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/secessentialshandson5) |

## Quick Links

Here are some quick links that may be useful when reviewing this Hands-On Guide.

- [GitLab Security Essentials Course Description](https://about.gitlab.com/services/education/security-essentials/)
- [GitLab Security Specialist Certification Details](https://about.gitlab.com/services/education/gitlab-security-specialist/)

## Suggestions

If you’d like to suggest changes to the *GitLab Security Essentials Hands-on Guide*, please submit them via merge request.
